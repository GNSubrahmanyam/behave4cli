================================
Behave 4 Command Line Interfaces
================================

.. image:: https://img.shields.io/pypi/v/behave4cli.svg
    :target: https://pypi.python.org/pypi/behave4cli

.. image:: https://gitlab.com/opinionated-digital-center/behave4cli/badges/master/pipeline.svg
    :target: https://gitlab.com/opinionated-digital-center/behave4cli/pipelines
    :alt: Linux build

This project provides an extension (testing domain) of `Behave`_ to test commands and
command-line interfaces (console apps). It is a packaging of the
``behave4cmd0`` `code <https://github.com/behave/behave/tree/v1.2.6/behave4cmd0>`_ created
by the Behave team.

The code has only been slightly modified from the fully working original code to pass
linting (`Flake8`_) and formatting (`isort`_ and `Black`_) tests.

First priority was to make the original (again, fully working) code and
features available as a library, hence efforts to create a comprehensive documentation
and test suite on existing code will depend on prioritisation and community
contribution. However, any additional feature will be fully tested (as well as
potentially affected existing code wherever feasible).

Having said that, extensive use of the steps is made in the `Behave bdd tests`_,
which can serve as example. This library's own bdd tests can be a starting point,
although, as said, it is incomplete.

* Free software license: BSD 2-clause

Features
--------

* Create a clean working directory
* Create and delete environment variables
* Run commands and check success/failure, return code and output
* Create, delete and check existence of directories
* Create, delete, check existence and check content of files
* Check for current date and working directory paths in commands output and
  files content

How to use
----------

Import the steps you want to use by creating a python file (we name it by convention
``use_steplib_behave4cli.py``) in your ``features/steps`` directory::

  # -- REGISTER-STEPS FROM STEP-LIBRARY:
  import behave4cli.__all_steps__

  # Behave self-test specific steps (not needed in most cases)
  import behave4cli.failing_steps
  import behave4cli.note_steps
  import behave4cli.passing_steps

Credits
-------

See ``LICENSE`` file for most code credits/copyrights.

This package was created with Cookiecutter_ and the
`opinionated-digital-center/cookiecutter-pypackage`_ project template.

.. _Behave: https://github.com/behave/behave
.. _Flake8: https://flake8.pycqa.org/en/latest/
.. _Black: https://black.readthedocs.io/en/stable/
.. _isort: https://timothycrosley.github.io/isort/
.. _Behave bdd tests: https://github.com/behave/behave/tree/v1.2.6/features
.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`opinionated-digital-center/cookiecutter-pypackage`: https://github.com/opinionated-digital-center/cookiecutter-pypackage
