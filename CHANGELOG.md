## [1.1.3](https://gitlab.com/opinionated-digital-center/behave4cli/compare/v1.1.2...v1.1.3) (2020-04-15)


### Bug Fixes

* improve confusing pass/fail assertion message ([f58b137](https://gitlab.com/opinionated-digital-center/behave4cli/commit/f58b1379c87c7b555dad6d9fd0301ad8f6de2b59))

## [1.1.2](https://gitlab.com/opinionated-digital-center/behave4cli/compare/v1.1.1...v1.1.2) (2020-04-11)


### Bug Fixes

* context.surrogate_text used only for first file created with content ([cd90bec](https://gitlab.com/opinionated-digital-center/behave4cli/commit/cd90bec9308c7d4f9122061f0572f41ebb8689cf))
* improve message on fail/pass with content ([9bb88ca](https://gitlab.com/opinionated-digital-center/behave4cli/commit/9bb88ca0e98f0bcca46d20354bb7808bcb320e8f))

## [1.1.1](https://gitlab.com/opinionated-digital-center/behave4cli/compare/v1.1.0...v1.1.1) (2020-04-10)


### Bug Fixes

* compare result before content in pass/fail with content ([4b8be7e](https://gitlab.com/opinionated-digital-center/behave4cli/commit/4b8be7e06871dc3c7c2c9e9f44bc586abcde5cb9))

# [1.1.0](https://gitlab.com/opinionated-digital-center/behave4cli/compare/v1.0.2...v1.1.0) (2020-03-31)


### Features

* can now use template subsitute {__TODAY_YYYY_MM_DD__} ([90500a7](https://gitlab.com/opinionated-digital-center/behave4cli/commit/90500a7015135799d3e100ea1070ac7b43f41c34))

## [1.0.2](https://gitlab.com/opinionated-digital-center/behave4cli/compare/v1.0.1...v1.0.2) (2020-03-30)


### Bug Fixes

* ensure __WORKDIR__ created at same level as features dir ([12ca8d1](https://gitlab.com/opinionated-digital-center/behave4cli/commit/12ca8d1de5c724fea1e67735fa4ddd9df2aead15))

## [1.0.1](https://gitlab.com/opinionated-digital-center/behave4cli/compare/v1.0.0...v1.0.1) (2020-03-30)


### Bug Fixes

* **release:** added forgotten CHANGELOG.md ([0bad211](https://gitlab.com/opinionated-digital-center/behave4cli/commit/0bad21140ad9bfffbf733be15acb3b09a0538546))
