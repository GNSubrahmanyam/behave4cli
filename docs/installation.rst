.. highlight:: shell

============
Installation
============


Stable release
--------------

To install Behave 4 Command Line Interfaces, run this command in your terminal:

.. code-block:: console

    $ pip install behave4cli

This is the preferred method to install Behave 4 Command Line Interfaces, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for Behave 4 Command Line Interfaces can be downloaded from the `GitLab repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone https://gitlab.com/opinionated-digital-center/behave4cli

Or download the `tarball`_:

.. code-block:: console

    $ curl -OJL https://gitlab.com/opinionated-digital-center/behave4cli/tarball/master

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _GitLab repo: https://gitlab.com/opinionated-digital-center/behave4cli
.. _tarball: https://gitlab.com/opinionated-digital-center/behave4cli/tarball/master
