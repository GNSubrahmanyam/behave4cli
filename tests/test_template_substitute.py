import os
from datetime import datetime

from hamcrest import assert_that, equal_to
from mock import Mock

from behave4cli import command_util


def test_template_substitute_directories():
    # Given
    context = Mock()
    context.workdir = os.path.join("a", "test", "workdir")
    source_text = """
The workdir is: {__WORKDIR__}
The cwd is: {__CWD__}
"""

    # When
    result_text = command_util.template_substitute_builtin_expressions(
        context, source_text
    )

    # Then
    expected_text = f"""
The workdir is: {context.workdir}
The cwd is: {os.getcwd()}
"""
    assert_that(result_text, equal_to(expected_text))


def test_template_substitute_date():

    # Given
    context = Mock()
    context.workdir = ""
    source_text = """
The date is: {__TODAY_YYYY_MM_DD__}
"""

    # When
    # Make sure that we are not extremely unlucky and have a date change by the time
    # we call the method :)
    while True:
        today_yyyy_mm_dd = datetime.today().strftime("%Y-%m-%d")
        result_text = command_util.template_substitute_builtin_expressions(
            context, source_text
        )
        if today_yyyy_mm_dd == datetime.today().strftime("%Y-%m-%d"):
            break

    # Then
    expected_text = f"""
The date is: {today_yyyy_mm_dd}
"""
    assert_that(result_text, equal_to(expected_text))
