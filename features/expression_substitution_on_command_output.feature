Feature: Replace expressions in command output

    Background: Setup a working directory
        Given a new working directory

    Scenario: Command output should contain workdir when in workdir
        When I run "python -c 'import os; print(os.path.abspath("."))'"
        Then the command output should contain "{__WORKDIR__}"

    Scenario: Command output should contain workdir when in dir inside workdir
        Given a directory named "test_dir"
        When I run "python -c 'import os; print(os.path.abspath("test_dir"))'"
        Then the command output should contain "{__WORKDIR__}"

    Scenario: Command output should contain cwd
        When I run "python -c 'import os; print(os.getcwd())'"
        Then the command output should contain "{__CWD__}"

    Scenario: Command output should not contain workdir
        When I run "python -c 'print("foo")'"
        Then the command output should not contain "{__WORKDIR__}"

    Scenario: Command output should not contain cwd
        When I run "python -c 'print("foo")'"
        Then the command output should not contain "{__CWD__}"

    Scenario: Command output should contain workdir n times
        When I run "python -c 'import os; print(os.path.abspath(".")); print(os.path.abspath("."))'"
        Then the command output should contain "{__WORKDIR__}" 2 times

    Scenario: Command output should contain exactly workdir subdir
        Given a directory named "test_dir"
        When I run "python -c 'import os; print(os.path.abspath("test_dir"))'"
        # Yes, this is the output expected when looking at existing code
        Then the command output should contain exactly "{__WORKDIR__}"

    Scenario: Command output should not contain exactly workdir
        When I run "python -c 'print("foo")'"
        Then the command output should not contain exactly "{__WORKDIR__}"
