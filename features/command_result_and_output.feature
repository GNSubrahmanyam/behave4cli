Feature: Verify command result and output

    @setup
    Scenario: Setup a working directory
        Given a new working directory

    Scenario: Command should pass
        When I run "python -c 'exit(0)'"
        Then it should pass

    Scenario: Command should pass
        When I run "python -c 'exit(1)'"
        Then it should fail

    Scenario: Command should pass with content
        When I run "python -c 'print("This is my content"); exit(0)'"
        Then it should pass with
            """
            This is my content
            """

    Scenario: Command should fail with content
        When I run "python -c 'print("This is my content"); exit(1)'"
        Then it should fail with
            """
            This is my content
            """
