Feature: Create and clean a working directory

    Scenario: Create a working directory
        Given a new working directory
        Then the directory "../__WORKDIR__" exists

    Scenario: Clean existing working directory
        Given a new working directory
        And an empty file named "a_directory/empty_file"
        And a new working directory
        Then the file named "a_directory/empty_file" should not exist
        And the directory "a_directory" should not exist

